import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(HttpDemoApp());
}

class HttpDemoApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Http Demo App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HttpDemoAppPage(),
    );
  }
}

class HttpDemoAppPage extends StatefulWidget {
  HttpDemoAppPage({Key? key}) : super(key: key);

  @override
  _HttpDemoAppPageState createState() => _HttpDemoAppPageState();
}

class _HttpDemoAppPageState extends State<HttpDemoAppPage> {
  List widgets = [];

  @override
  void initState() {
    super.initState();
    loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Http Demo"),
      ),
      body: ListView.builder(
        itemCount: widgets.length,
        itemBuilder: (BuildContext context, int position) {
          return getRow(position);
        },
      ),
    );
  }

  Widget getRow(int i) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Text("Row ${widgets[i]["title"]}"),
    );
  }

  Future<void> loadData() async {
    Uri dataURL = Uri.parse("https://jsonplaceholder.typicode.com/posts");
    http.Response response = await http.get(dataURL);
    setState(() {
      widgets = jsonDecode(response.body);
    });
  }
}