import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

import 'demo_animate1.dart';
import 'demo_async1.dart';
import 'demo_isolate1.dart';
import 'demo_lifecycle1.dart';
import 'demo_paint1.dart';

const String routeDemoAnimate1 = '/DemoAnimate1';
const String routeDemoPaint1 = '/DemoPaint1';
const String routeDemoHttp1 = '/DemoHttp1';
const String routeDemoIsolate1 = '/routeDemoIsolate1';
const String routeDemoLifecycle1 = '/routeDemoLifecycle1';

void main() => runApp(MyApp()); //arrow notation for one-line functions or methods.

class MyApp extends StatelessWidget { //Stateless widgets are immutable, meaning that their properties can’t change—all values are final.
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Welcome to Flutter',
      theme: ThemeData(
        primaryColor: Colors.white,
      ),
      home: RandomWords(),
      routes: <String, WidgetBuilder> {
        routeDemoAnimate1: (BuildContext context) => MyFadeTest(title: 'Fade Demo'),
        routeDemoPaint1: (BuildContext context) => PaintDemoApp(),
        routeDemoHttp1: (BuildContext context) => HttpDemoApp(),
        routeDemoIsolate1: (BuildContext context) => DemoIsolateApp(),
        routeDemoLifecycle1: (BuildContext context) => DemoLifecycleWatcher(), //Center(child: LifecycleWatcher()),
      },
    );
  }
}

class RandomWords extends StatefulWidget {
  const RandomWords({Key? key}) : super(key: key);

  @override
  _RandomWordsState createState() => _RandomWordsState();
}

class _RandomWordsState extends State<RandomWords> {
  final _suggestions = <WordPair>[];
  final _saved = <WordPair>{};
  final _biggerFont = TextStyle(fontSize: 18.0);

  @override
  Widget build(BuildContext context) {
    final wordPair = WordPair.random();
    return Scaffold(
      appBar: AppBar(
        title: Text('Startup Name Generator'),
        actions: [
          IconButton(onPressed: _pushSaved, icon: Icon(Icons.list)),
        ],
      ),
      body: _buildSuggestions(),
      floatingActionButton: FloatingActionButton(
        onPressed: _toggle,
        tooltip: 'Update Text',
        child: _getToggleChild(),
      ),
    );
  }

  void _pushSaved() {
    Navigator.of(context).pushNamed(routeDemoPaint1);
    // Navigator.of(context).pushNamed(routeDemoLifecycle1);

    // Navigator.of(context).push(
      // MaterialPageRoute(builder: (BuildContext) {
      //   final tiles = _saved.map( (WordPair pair) {
      //     return ListTile(
      //       title: Text(pair.asPascalCase, style: _biggerFont,),
      //     );
      //   } );
      //   final divided = ListTile.divideTiles(tiles: tiles, context: context).toList();
      //
      //   return Scaffold(
      //     appBar: AppBar(title: Text('Saved Suggestions'),),
      //     body: ListView(children: divided,),
      //   );
      // },
      // ),
    // );
  }

  Widget _buildSuggestions() {
    return ListView.builder(
      padding: EdgeInsets.all(16.0),
      itemBuilder: (context, i) {
        if(i.isOdd) return Divider();

        final index = i ~/ 2;
        if(index >= _suggestions.length) {
          _suggestions.addAll(generateWordPairs().take(10));
        }
        return _buildRow(_suggestions[index]);
      },

    );
  }

  Widget _buildRow(WordPair pair) {
    final alreadySaved = _saved.contains(pair);
    return ListTile(
      title: Text(
        pair.asPascalCase,
        style: _biggerFont,
      ),
      trailing: Icon(
        alreadySaved ? Icons.favorite : Icons.favorite_border,
        color: alreadySaved ? Colors.red : null,
      ),
      onTap: () {
        setState( () {
          if(alreadySaved) {
            _saved.remove(pair);
          } else {
            _saved.add(pair);
          }
        });
      },
    );
  }

  bool toggle = true;
  void _toggle() {
    setState(() {
      toggle = !toggle;
    });
  }

  _getToggleChild() {
    if (toggle) {
      return Text('Toggle One');
    } else {
      return ElevatedButton(onPressed: _toggle, child: Text('Toggle Two'));
    }
  }
}
