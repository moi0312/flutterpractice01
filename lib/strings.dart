import 'package:intl/intl.dart';

class Strings {
  static String welcomeMessage = "Welcome To Flutter";
  static greetingMessage(name) => Intl.message(
      'Hello $name!',
      locale: "zh",
      name: 'greetingMessage',
      args: [name],
      desc: 'Greet the user as they first open the application',
      examples: const {'name': 'Emily'});
}