import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class LifecycleWatcher extends StatefulWidget {
  @override
  _LifecycleWatcherState createState() => _LifecycleWatcherState();
}

class _LifecycleWatcherState extends State<LifecycleWatcher> with WidgetsBindingObserver {
  AppLifecycleState? _lastLifecycleState;

  final _lastLifecycles = <String>[];

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    setState(() {
      _lastLifecycleState = state;
      print(state);
      _lastLifecycles.add(state.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('生命週期'),
      ),
      body: _buildListView(),
    );

  }

  Widget _buildListView() {
    if (_lastLifecycleState == null)
      return Center(child: Text('這個 widget 未觀察到任何生命週期更改.', textDirection: TextDirection.ltr));

    // return Text('這個 widget 觀察到的最新生命週期狀態是: $_lastLifecycleState.',
    //     textDirection: TextDirection.ltr);
    return ListView.builder(
      itemCount: _lastLifecycles.length * 2,
      itemBuilder: (context, i) {
        if(i.isOdd) return Divider();

        final index = i ~/ 2;
        return _buildRow(_lastLifecycles[index]);
      },
      padding: null,
    );
  }

  Widget _buildRow(String cycle) {
    return ListTile(
      title: Text(
        cycle,
        style: TextStyle(fontSize: 12.0),
      ),
    );
  }


}


class DemoLifecycleWatcher extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fade Demo',

      home: LifecycleWatcher(),
    );
  }
}

void main() {
  runApp(DemoLifecycleWatcher());
}